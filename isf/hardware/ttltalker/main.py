import serial
import serial.tools.list_ports
from isf.core import logger


class TTLTalker:
    baudrate = 9600
    com_port = '/dev/ttyACM0'
    serial_link = None
    timeout = 0.1

    def __init__(self, com_port="/dev/ACM0", baudrate=115200, timeout=0.1):
        self.baudrate = baudrate
        self.com_port = com_port
        self.timeout = timeout
        if self.serial_link is None or not self.serial_link.is_open:
            self.connect()

    def __del__(self):
        if not (self.serial_link is None) and self.serial_link.is_open:
            self.serial_link.close()

    def connect(self):
        try:
            self.serial_link = serial.Serial(self.com_port, self.baudrate,
                                             timeout=self.timeout)
        except Exception as e:
            logger.error('Can\'t connect to {}'.format(self.com_port),
                         exc_info=None)
            logger.error('List of devices: ' + ','.join(
                [port.device for port in serial.tools.list_ports.comports()]))

    def read_output(self, length):
        if self.serial_link is None or not self.serial_link.is_open:
            logger.error('Port {} not open!'.format(self.com_port))
            return
        s = self.serial_link.read(length)
        result = {'ret_str': s}
        return result

    def send_char(self, char_hex, output=True, output_length=10000):
        if self.serial_link is None or not self.serial_link.is_open:
            logger.error('Port {} not open!'.format(self.com_port))
            return
        self.serial_link.write(chr(int(char_hex, 16)).encode())
        result = {}
        if output:
            result = self.read_output(output_length)
        return result

    def send_bytes(self, str_hex, output=True, output_length=10000,
                   newline_bytes=""):
        if self.serial_link is None or not self.serial_link.is_open:
            logger.error('Port {} not open!'.format(self.com_port))
            return
        self.serial_link.write(bytes.fromhex(str_hex + newline_bytes))
        result = {}
        if output:
            result = self.read_output(output_length)
        return result  # result = {'ret_str': s}
